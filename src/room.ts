class Room {
    id: number;
    description: string;
    studiepunt: number;
    locked: boolean;

    northExit: Room;
    southExit: Room;
    eastExit: Room;
    westExit: Room;

    //Optional studiepunt!
    constructor(id: number, description: string, locked: boolean, studiepunt?: number) {
        if (studiepunt) {
            this.id = id;
            this.description = description;
            this.studiepunt = studiepunt;
            this.locked = locked;
        } else {
            this.id = id;
            this.description = description;
            this.locked = locked;
        }
    }

    setExits(north: Room, east: Room, south: Room, west: Room): void {
        if (north != null) {
            this.northExit = north;
        }
        if (east != null) {
            this.eastExit = east;
        }
        if (south != null) {
            this.southExit = south;
        }
        if (west != null) {
            this.westExit = west;
        }
    }
}

