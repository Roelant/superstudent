class Parser {
    input: HTMLInputElement;
    game: Game;
    commands: { [key: string]: Command } = {};
    default: Default;

    constructor(game: Game, input: HTMLInputElement) {
        this.game = game;
        this.input = input;
        this.default = new Default(game);
        this.commands["go"] = new Go(game);
        this.commands["grab"] = new Grab(game);
        this.commands["help"] = new Help(game);
        this.commands["joke"] = new Joke(game);
        this.commands["look"] = new Look(game);
        this.commands["rugzak"] = new Rugzak(game);
        this.commands["studiepunt"] = new Studiepunt(game);
        this.commands["uitschrijven"] = new Uitschrijven(game);
        this.commands["unlock"] = new Unlock(game);
        this.commands["where"] = new Where(game);

        let enter = 13; //enter key
        input.onkeyup = (e) => {
            if (e.keyCode == enter && this.game.isOn) {
                let command = this.input.value;
                this.game.out.println(command);
                this.parse(command.split(" "));
                this.input.value = ""; // clears the input element 
                this.game.out.print(">");
            }
        }
    }

    parse(words: string[]): void {
        let wantToQuit = false;
        let params = words.slice(1);
        if (words[0] == "") {
            return;
        }

        let command: Command;
        command = this.commands[words[0]];
        if (command == null) {
            command = this.default;
        }
        wantToQuit = command.execute(params);

        if (wantToQuit) {
            this.input.disabled = true;
            this.game.gameOver();
        }
    }

}