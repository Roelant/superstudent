class Printer {
    output: HTMLElement;

    constructor(output: HTMLElement) {
        this.output = output;
    }

    print(text: string): void {
        this.output.innerHTML += text;
    }

    println(text = ""): void { //White color.
        this.print(text + "<br/>");
        this.output.scrollTop = this.output.scrollHeight;
    }

    printError(text = ""): void { //Red color
        this.print("<span class='error'>" + text + "</span><br/>");
        this.output.scrollTop = this.output.scrollHeight;
    }
    printKey(text = ""): void { //Yellow color
        this.print("<span class='key'>Sleutel gevonden: " + text + "</span><br/>");
        this.output.scrollTop = this.output.scrollHeight;
    }

    printInv(text = ""): void { // Blue color
        this.print("<span class='inv'>Je rugzak bevat: " + text + "</span><br/>");
        this.output.scrollTop = this.output.scrollHeight;
    }

    printsuccess(text = ""): void { //Green color
        this.print("<span class='succes'>Succes: " + text + "</span><br/>");
        this.output.scrollTop = this.output.scrollHeight;
    }
}