
class Game {
    parser: Parser;
    out: Printer;
    currentRoom: Room;
    nextRoom: Room;
    isOn: boolean;
    studiepunt: number;
    allRooms: Array<Room> = [];
    visitedRooms: Array<number> = [];
    inventory: Array<String> = [];

    constructor(output: HTMLElement, input: HTMLInputElement) {
        this.parser = new Parser(this, input);
        this.out = new Printer(output);
        this.isOn = true;
        this.createRooms();
        this.studiepunt = 0;
        this.printWelcome();
    }
    //Add studiepunten to the player.
    addstudiepunt(studiepunt: number): void {
        let total = this.studiepunt + studiepunt;
        this.studiepunt = total;
    }
    //Create the rooms, with ID, description, locked and optional the amound of studiepunten.
    createRooms(): void {
        // create the rooms
        let psd = new Room(0, "in het PSD gebouw. Het is hier koud, leeg en bijna uitgestorven. Er liggen dode muizen in de lift. Legendes hadden hier ooit nog les. Tussen de dode muizen zie je iets glinsteren het is een sleutel. Pak deze op met <span class='key'>'grab'</span>. Je vind onder een lijk van een andere super student 50 studiepunten.", false, 50);
        let outside = new Room(1, "voor de HZ. Dit is het begin van je avontuur! Denk je dat je de juiste studie heb gekozen? We zullen het zien!", false);
        let gebouw = new Room(2, "in het gebouw. Welkom op de Hogeschool Zeeland! Volgens mij ben jij een ICT student. Aan je rechterkant kan jij je melden bij de balie.", true);
        let smoske = new Room(3, "in het Smoske. Je hebt honger, heel veel honger. Al deze lekkere broodjes, maar welke kies je? Smos kaas & hesp of een javaanse kip. De keuze is oneinding. Je besluit om een broodje Smos te kopen.", false, 20);
        let balie = new Room(4, "bij de balie. Hallo student, leuk dat je je meld bij de balie. Loop een rondje door het gebouw, maar vermijd de helpdesk!!!!. Je vind hier je toelating voor de ict opleiding. Pak deze op met <span class='key'>'grab'</span>. ", false, 20);
        let vleugelPE = new Room(5, "bij de PE vleugel. Je hoort dat er hier engels wordt gegeven voor ICT studenten. Het is maar beter om hier weg te gaan...", false, 10);
        let vleugelA = new Room(6, "bij de A vleugel. Je ziet in de verte een lichtje branden, is dat de verdoemde helpdesk? Gebruik eventueel <span class='green'>look (richting)</span> om zeker te weten wat daar is.", false);
        let helpdesk = new Room(7, "bij de helpdesk. Komop je bent een ICT student. Je weet alles te vinden via google. Moest je nou echt hulp hebben. We vragen ons af of deze studie wel iets voor jou is...", false, -20);
        let kantine = new Room(8, "bij de kantine. Ahhh ik zie dat je honger hebt. Neem een heerlijk broodje, maar niet te gezond. Je bent wel een ICT student eh. Wist je dat het Smoske ook lekker is? Deze is net buiten de HZ!", false);
        let broodjeskelder = new Room(9, "bij de geheime broodjes kelder. Je komt in een geheime kamer. Je vind hier 10 studiepunten in een oud broodje.", false, 10);
        let vleugelV = new Room(10, "bij de V vleugel. Je komt hier meiden tegen, je bent bang, je keert terug. 20 studiepunten voor de moeite...", false, 20);
        let atrium = new Room(11, "bij het Atrium. Je vind het hier te rumoerig. Je pakt snel 10 studiepunten op die achter een piano liggen.", false, 10);
        let ict = new Room(12, "bij het ICT lokaal. Ahhh ik zie dat je de ICT lokalen heb gevonden. Welkom op de opleiding ICT!", true, 20);
        let koffiekamer = new Room(13, "bij de koffiekamer. Hier staat het heilige koffieapparaat wat je nog de rest van je opleiding nodig gaat hebben. Je besluit om een kopje koffie te pakken en verdient hiermee 20 studiepunten.", false, 20);
        let wc = new Room(14, "in de wc's. Je leest hier dat iemand gestopt is met zijn opleiding. Je neemt zijn studiepunten over...", false, 20);
        let kantoren = new Room(15, "bij de kantoren. Je hebt de kantoren van de docenten gevonden!", false);
        let rimmert = new Room(16, "bij Rimmerts kantoor. Je komt hier Rimmert tegen. Je ziet allemaal posters van Javascript en Macbooks aan de muur hangen. Je vind dit maar, je besluit om terug te gaan.", false, 10);
        let anton = new Room(17, "bij Antons kantoor. Je betrapt anton met zijn rad van termen! Anton heeft straks een gastcollege en draait snel aan zijn rad van termen. Je moet spontaan een domeinmodel maken, Ik zou maar snel weggaan hier....", false);
        let daan = new Room(18, "bij Daans kantoor. Je zoekt je SLC, maar hij is er niet? Je kijkt goed rond, je ziet in het zuiden een kleine doorgang, wat zou daar zitten?", false);
        let daanskelder = new Room(19, "bij daans kelder. Je komt hier Daan tegen, daan zit weer te kijken naar zijn viezeplaatjes. Hij geeft je 50 studiepunten om hier over te zwijgen... Je keert weer terug....", false, 50);
        
        //Add the exits
        psd.setExits(outside, null, null, null);
        outside.setExits(gebouw, smoske, psd, null);
        gebouw.setExits(vleugelA, balie, outside, atrium);
        smoske.setExits(null, null, null, outside);
        balie.setExits(vleugelPE, null, null, gebouw);
        vleugelPE.setExits(null, null, balie, vleugelA);
        vleugelA.setExits(helpdesk, vleugelPE, gebouw, kantine);
        helpdesk.setExits(null, null, vleugelA, null);
        kantine.setExits(broodjeskelder, vleugelA, atrium, vleugelV);
        broodjeskelder.setExits(null, null, kantine, null);
        vleugelV.setExits(null, kantine, null, null);
        atrium.setExits(kantine, gebouw, ict, null);
        ict.setExits(atrium, koffiekamer, wc, kantoren);
        koffiekamer.setExits(null, null, null, ict);
        wc.setExits(ict, null, null, null);
        kantoren.setExits(rimmert, ict, anton, daan);
        rimmert.setExits(null, null, kantoren, null);
        anton.setExits(kantoren, null, null, null);
        daan.setExits(null, kantoren, daanskelder, null);
        daanskelder.setExits(daan, null, null, null);

        //Push all the rooms in a array.
        this.allRooms.push(psd, outside, gebouw, smoske, balie, vleugelPE, vleugelA, helpdesk, kantine, broodjeskelder, vleugelV, atrium, ict, koffiekamer, wc, kantoren, rimmert, anton, daan, daanskelder);

        //Set the currentroom to outside.
        this.currentRoom = outside;
        this.visitedRooms.push(this.currentRoom.id);
    }
    printWelcome(): void {
        this.out.println("Welkom student!");
        this.out.println("Jouw doel is om <span class='green'>240</span> studiepunten te halen. Deze vind je in de kamers.");
        this.out.println("Op dit moment heb je <span class='green'>" + this.studiepunt + "</span> studiepunten");
        this.out.println("Typ 'help' als je hulp nodig hebt.");
        this.out.println("Ga naar een kamer door 'go {richting}' te typen. Bijvoorbeeld go north.");
        this.out.println(); 
        this.out.println("Je bent nu " + this.currentRoom.description);
        this.out.print("Uitgangen: ");

        if (this.currentRoom.northExit != null) {
            this.out.print("north ");
        }
        if (this.currentRoom.eastExit != null) {
            this.out.print("east ");
        }
        if (this.currentRoom.southExit != null) {
            this.out.print("south ");
        }
        if (this.currentRoom.westExit != null) {
            this.out.print("west ");
        }
        this.out.println();
        this.out.print(">");
    }

    gameOver(): void {
        this.isOn = false;
        this.out.println("Helaas, veel studenten vallen af!");
        this.out.println("Je bent succesvol uitgeschreven!");
        window.open("https://duo.nl/particulier/student-hbo-of-universiteit/studeren/stoppen-met-je-studie.jsp");
    }

    gameWon(): void {
        this.isOn = false;
        window.open('win.html', '_self');
    }
}