class Unlock extends Command {
    //Execute the unlock command
    execute(params: string[]): boolean {
        if (params.length == 0) {
            this.game.out.printError("Welke deur wil je openen?");
            return;
        }
        //Get the direction
        let direction = params[0];

        let nextRoom = null;
        switch (direction) {
            case "north":
                nextRoom = this.game.currentRoom.northExit;
                break;
            case "east":
                nextRoom = this.game.currentRoom.eastExit;
                break;
            case "south":
                nextRoom = this.game.currentRoom.southExit;
                break;
            case "west":
                nextRoom = this.game.currentRoom.westExit;
                break;
        }

        //Check if the nextroom is null
        if (nextRoom == null) {
            this.game.out.println("<span class='error'>Deze deur kan jij niet openen!</span>");
        }
        //Check if the nextroom is locked.
        if (nextRoom.locked) {
            if (nextRoom.id == 2) { //ingang
                if (this.game.inventory.indexOf("Sleutel van de HZ") > -1) {
                    this.game.out.printsuccess("Je hebt succesvol de volgende kamer geopend!");
                    nextRoom.locked = false; //This opens the door
                    this.game.inventory.splice(0, 1); //Remove the key
                    return;
                }
            }
            if (nextRoom.id == 12) { //Ict lokalen
                if (this.game.inventory.indexOf("Toelating tot de ICT opleiding") > -1) {
                    this.game.out.printsuccess("Je hebt succesvol de volgende kamer geopend!");
                    nextRoom.locked = false;//This opens the door
                    this.game.inventory.splice(0, 1);//Remove the key
                    return;
                }
            }
            this.game.out.printError("Je hebt geen sleutel voor deze kamer, zoek deze eerst!");
        } else {
            this.game.out.printError("Deze deur is al open!");
        }
        return false;
    }

}