
class Studiepunt extends Command {
    //Execute the studiepunt command.
    execute(params: string[]): boolean {
        if (params.length > 0) {
            this.game.out.println("Wow teveel argumenten!");
            return false;
        }
        this.game.out.println("Je hebt op dit moment <span class='green'>" + this.game.studiepunt + " </span>studiepunten.");
        return false;
    }
}
