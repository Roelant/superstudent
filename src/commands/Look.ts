class Look extends Command {
   //Execute the look command
    execute(params: string[]): boolean {
        if (params.length == 0) {
            //Check the params.
            this.game.out.println("Look {richting}?");
            return;
        }

        let direction = params[0];
        // Try to leave current room.
        let nextRoom = null;
        switch (direction) {
            case "north":
                nextRoom = this.game.currentRoom.northExit;
                break;
            case "east":
                nextRoom = this.game.currentRoom.eastExit;
                break;
            case "south":
                nextRoom = this.game.currentRoom.southExit;
                break;
            case "west":
                nextRoom = this.game.currentRoom.westExit;
                break;
        }
        //Check if the room is null
        if (nextRoom == null) {
            this.game.out.println("<span class='error'>Daar kan je niet naar kijken!</span>");
        }
        else {
            //send the room messages
            this.game.nextRoom = nextRoom;
            this.game.out.println("De volgende kamer is " + this.game.nextRoom.description);
        }
        return false;
    }
}