
class Joke extends Command {
    public allJokes: Array<string> = [];
    private lastjokeId: number;
    //Create an Array of all the jokes.
    private addJokes(): void {
        this.allJokes.push("Het is een moeilijke tijd voor Badr Hari, maar hij slaat zich er wel doorheen");
        this.allJokes.push("Waarom drinken muizen geen alcohol? Omdat ze bang zijn voor een kater.");
        this.allJokes.push("Wat is het verschil tussen een batterij en een vrouw ?...... Aan een batterij zit ook een positieve kant.");
        this.allJokes.push("Wat is rood en ligt op het strand?...... ' Roodkrabje'!!");
        this.allJokes.push("Een speciaal ei is een eggsclusive..");
        this.allJokes.push("Betaalt Bjorn Borg of heeft Calvin Kleingeld? ");
        this.allJokes.push("Weet je waar Rijk's water staat?");
        this.allJokes.push("Wat staat er op een graf van een computer? Game over..");
        this.allJokes.push("Het is groen en het zit op een hek? Verf.");
        this.allJokes.push("Het is groen en het ontploft? boemkool");
    }
    //Generate a random Integer between the max of all jokes and returns id.
    private randomInt(): number {
        let id = Math.floor(Math.random() * this.allJokes.length);
        if (id == this.lastjokeId) {
            return this.randomInt();
        }
        this.lastjokeId = id;
        return id;
    }

    execute(params: string[]): boolean {
        if (params.length > 0) {
            this.game.out.println("Te veel argumenten!");
            return;
        }
        if (this.allJokes.length == 0) {
            this.addJokes();
        }
        let id = this.randomInt();
        this.game.out.print(this.allJokes[id] + "</br>");
        return false;
    }
}
