class Grab extends Command {
    //Execute the grab command
    KEY_AL_OPGEPAKT: string = "Je hebt deze sleutel al opgepakt.";
    BEKIJK_RUGZAK: string = "Bekijk je rugzak met 'rugzak'";
    KEY_NAME_SLEUTEL: string = "Sleutel van de HZ";
    KEY_NAME_DIPLOMA: string = "Toelating tot de ICT opleiding";

    execute(params: string[]): boolean {
        //Check if the params is greater then 0
        if (params.length > 0) {
            this.game.out.printError("Teveel argumenten");
            return;
        }
        //Grab in the PSD building
        if (this.game.currentRoom.id == 0) {
            if (this.game.inventory.indexOf(this.KEY_NAME_SLEUTEL) > -1) {
                this.game.out.printError(this.KEY_AL_OPGEPAKT);
                return;
            }
            this.game.inventory.push(this.KEY_NAME_SLEUTEL);
            this.game.out.printKey("Je vind hier een " + this.KEY_NAME_SLEUTEL + ", hiermee kan je een deur openen!");
            this.game.out.println(this.BEKIJK_RUGZAK);
            return false;
        }

        //Grab in the room Balie
        if (this.game.currentRoom.id == 4) {
            if (this.game.inventory.indexOf(this.KEY_NAME_DIPLOMA) > -1) {
                this.game.out.printError(this.KEY_AL_OPGEPAKT);
                return;
            }
            this.game.inventory.push(this.KEY_NAME_DIPLOMA);
            this.game.out.printKey("Je vind hier een diploma.");
            this.game.out.println(this.BEKIJK_RUGZAK);
            return false;
        }
        else {
            this.game.out.printError("Je kan hier niks oppakken!");
        }

    }

}
