class Help extends Command {
    //Execute the Help command
    execute(params: string[]): boolean {
        //Check if the params is greater then 0
        if (params.length > 0) {
            this.game.out.printError("Wow teveel argumenten!");
            return false;
        }
        //Send the message
        this.game.out.println("Je bent op zoek naar 240 studiepunten. Op dit moment heb je er <span class='green'>" + this.game.studiepunt + " </span>studiepunten.")
        this.game.out.println("Je hebt de volgende uitgangen waar je naar toe kan: ")
        if (this.game.currentRoom.northExit != null) {
            this.game.out.print("north, ");
        }
        if (this.game.currentRoom.eastExit != null) {
            this.game.out.print("east, ");
        }
        if (this.game.currentRoom.southExit != null) {
            this.game.out.print("south, ");
        }
        if (this.game.currentRoom.westExit != null) {
            this.game.out.print("west, ");
        }
        this.game.out.println();
        this.game.out.println("Gebruik <span class='key'>look</span> om in de volgende kamer te kijken, of gebruik <span class='key'>where</span> om te kijken waar je nu bent.")
        return false;
    }
}