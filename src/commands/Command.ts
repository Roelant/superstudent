class Command {
    //Super class
    protected game: Game;

    constructor(game: Game) {
        this.game = game;
    }
    //Default function override in subclass
    execute(params: string[]): boolean {
        return false;
    }
}  