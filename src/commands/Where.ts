
class Where extends Command {
    //Execute the Where command
    execute(params: string[]): boolean {
        if (params.length > 0) {
            this.game.out.println("Te veel argumenten!");
            return;
        }
        //check where the player is and send the exits.
        let direction = params[0];
        this.game.out.println("Je bent nu " + this.game.currentRoom.description);

        this.game.out.print("Uitgangen: ");
        if (this.game.currentRoom.northExit != null) {
            this.game.out.print("north ");
        }
        if (this.game.currentRoom.eastExit != null) {
            this.game.out.print("east ");
        }
        if (this.game.currentRoom.southExit != null) {
            this.game.out.print("south ");
        }
        if (this.game.currentRoom.westExit != null) {
            this.game.out.print("west ");
        }
        this.game.out.println();
    }
}
