class Go extends Command {
    //Execute the go command
    execute(params: string[]): boolean {
        if (params.length == 0) {
            this.game.out.printError("Waar wil je naar toe?");
            return;
        }
        //Get the direction
        let direction = params[0];

        let nextRoom = null;
        switch (direction) {
            case "north":
                nextRoom = this.game.currentRoom.northExit;
                break;
            case "east":
                nextRoom = this.game.currentRoom.eastExit;
                break;
            case "south":
                nextRoom = this.game.currentRoom.southExit;
                break;
            case "west":
                nextRoom = this.game.currentRoom.westExit;
                break;
        }

        //Check if the nextroom is null
        if (nextRoom == null) {
            this.game.out.println("<span class='error'>Daar kan je niet naar toe!</span>");
        }

        else {
            //Check if the nextroom is locked
            if (nextRoom.locked) {
                this.game.out.printError("Je hebt een sleutel nodig voor deze kamer. Gebruik unlock {richting}");
                return false;
            }
            //Set the currentroom
            this.game.currentRoom = nextRoom;
            this.game.out.println("Je bent nu " + this.game.currentRoom.description);
            //Check if the player already visited the current room
            if (this.game.visitedRooms.indexOf(this.game.currentRoom.id) < 0) {
                //Check if the room has a studiepiunt
                if (this.game.currentRoom.studiepunt) {
                    //Check if the studiepunt is greater then 0, then add and send a green message
                    if (this.game.currentRoom.studiepunt > 0) {
                        this.game.out.println("Je vind hier <span class='green'>" + this.game.currentRoom.studiepunt + "</span> studiepunten");
                        //else we send an red message
                    } else {
                        this.game.out.println("Je vind hier <span class='error'>" + this.game.currentRoom.studiepunt + "</span> studiepunten");
                    }
                    this.game.addstudiepunt(this.game.currentRoom.studiepunt);
                    if (this.game.studiepunt > 239) {
                        this.game.gameWon();
                    }
                }
            }
            //Add the currentroom to the visitedrooms array
            this.game.visitedRooms.push(this.game.currentRoom.id);
            //Send all the exits
            this.game.out.print("Uitgangen: ");
            if (this.game.currentRoom.northExit != null) {
                this.game.out.print("north ");
            }
            if (this.game.currentRoom.eastExit != null) {
                this.game.out.print("east ");
            }
            if (this.game.currentRoom.southExit != null) {
                this.game.out.print("south ");
            }
            if (this.game.currentRoom.westExit != null) {
                this.game.out.print("west ");
            }
            this.game.out.println();
        }
        return false;
    }
}