# Super Student #
Dit is een text-based game waar je 240 studiepunten moet zoeken in kamers door de HZ, maar kijk uit voor de vele valkuilen! Vind je het een moeilijke game en ben je er helemaal klaar mee? Schrijf je dan uit. Typ: uitschrijven
## Live spelen zonder installatie: ##
Deze super vette game staat natuurlijk ook online op mijn eigen site! Ik heb de game opgenomen als 1 van mijn projecten. Kom je er niet uit? Open de plattegrond!

https://roelantdelooff.nl/projecten/super_student/index.php

## Commando's ##
1. go {richting}
2. grab
3. help
4. joke
5. look {richting}
6. rugzak
7. studiepunt
8. uitschrijven
9. unlock {richting}
10. where

## Snelste route om de game uit te spelen ##
Denk je van, "Ik moet weer een game beoordelen, kom ik zal hem eens spelen, maar ik heb geen zin om weer een hele game uit te spelen?" Volg dan de volgende commando's en hierdoor bekijk je alle functies van deze game!

1. go south
2. grab
3. rugzak
4. go north
5. unlock north
6. go east
7. go west
8. go north
9. go east
10. grab
11. go north
12. go west
13. look north (NIET NA TOE GAAN DAN VERLIES JE 20 studiepunten)
14. go west
15. go north
16. go south
17. go west
18. go east
19. go south
20. unlock south
21. go south
22. go east
23. go west
24. go south
25. go north
26. go west
27. go north
28. go south
29. go west
30. go south
Gefeliciteerd je hebt de game zo snel mogelijk uitgespeeld! Je hebt helaas niet alle commando's gebruikt. Kijk ook eens even onder de commando kop!